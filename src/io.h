#include "config.h"
#include "tinycthread.h"

#define MAX_TEXT_LENGTH 256

extern enum typing_mode { NOT_TYPING, TYPING_SIGN, TYPING_COMMAND, TYPING_LUA_RESPONSE } typing;
extern char typing_buffer[MAX_TEXT_LENGTH];
extern int message_index;
extern char messages[MAX_MESSAGES][MAX_TEXT_LENGTH];
extern mtx_t message_lock, typing_lock;

void init_io(void);
void add_message(char const *);	// actually this is in main.c
