#include "scripting.h"
#include "io.h"
#include "item.h"
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "tinycthread.h"
#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

static lua_State *lua;
static mtx_t world_lock, texture_lock, lua_state_lock;
static cnd_t typing_release;

#define LOCK(nm)	for (int zz = (mtx_lock(& nm ## _lock), 1); zz; mtx_unlock(& nm ## _lock), zz = 0)

static void error(lua_State *l, char const *context) {
	char msg[100];
	assert(strlen(context) < 80);
	sprintf(msg, "*** Lua error in '%s'", context);
	add_message(msg);
	add_message(lua_tostring(l, -1));
}

static int lua_get_parameters(lua_State *l, char const *format, ...) {
	va_list ap;
	va_start(ap, format);
	int num_args = 0;
	while (*format) {
		if (*format != '%')	return 0;
		format++;
		switch (*format) {
		case 'd':	if (!lua_isinteger(l, ++num_args)) {
						return 0;
					}
					*va_arg(ap, lua_Integer *) = lua_tointeger(l, num_args);
					break;
		case 'D':	// with 'D', non-int numbers are tolerated, but are rounded into ints
					num_args++;
					lua_Integer *r = va_arg(ap, lua_Integer *);
					if (lua_isinteger(l, num_args)) {
						*r = lua_tointeger(l, num_args);
					} else if (lua_isnumber(l, num_args)) {
						*r = (lua_Integer)(lua_tonumber(l, num_args) + 0.5);
					} else {
						return 0;
					}
					break;
		case 'f':	if (!lua_isnumber(l, ++num_args)) {
						return 0;
					}
					*va_arg(ap, lua_Number *) = lua_tonumber(l, num_args);
					break;
		case 's':	if (!lua_isstring(l, ++num_args)) {
						return 0;
					}
					*va_arg(ap, char const **) = lua_tostring(l, num_args);
					break;
		default:	return 0;
		}
		format++;
	}
	if (lua_gettop(l) != num_args) {
		return 0;
	}
	return 1;
}

static int lua_add_message(lua_State *l) {
	char *msg;
	if (!lua_get_parameters(l, "%s", &msg)) {
		return luaL_error(l, "print requires 1 argument");
	}
	add_message(msg);
	return 0;
}

static int lua_set_textures(lua_State *l) {
	lua_Integer b, fr, ba, to, bo, le, ri;
	if (!lua_get_parameters(l, "%d%d%d%d%d%d%d", &b, &fr, &ba, &to, &bo, &le, &ri)) {
		return luaL_error(l, "set_textures requires 7 integer arguments");
	}
	if (b < 1 || b > 9)	return luaL_error(l, "set_texture's block type must be between 1 and 9, not %ld", (long)b);
#define CHK(side)	if (side < 0 || side > 255)	{ \
						return luaL_error(l, "each texture must be between 0 and 255, not %ld", (long)side);	\
					}
	CHK(fr);
	CHK(ba);
	CHK(to);
	CHK(bo);
	CHK(le);
	CHK(ri);
#undef CHK
	LOCK(texture) {
		blocks[LUA_1 + b - 1][0] = fr;
		blocks[LUA_1 + b - 1][1] = ba;
		blocks[LUA_1 + b - 1][2] = to;
		blocks[LUA_1 + b - 1][3] = bo;
		blocks[LUA_1 + b - 1][4] = le;
		blocks[LUA_1 + b - 1][5] = ri;
	}
	Chunk *c = find_chunk(chunked(main_player->state.x),
		chunked(main_player->state.z));
	dirty_chunk(c);
	return 0;
}

static int lua_destroy_block(lua_State *l) {
	lua_Integer x, y, z;
	if (!lua_get_parameters(l, "%D%D%D", &x, &y, &z)) {
		return luaL_error(l, "destroy_block requires 3 integer arguments");
	}
	LOCK(world) {
		set_block(x, y, z, 0);
		record_block(x, y, z, 0);
	}
	return 0;
}

static int lua_create_lua_block(lua_State *l) {
	lua_Integer b, x, y, z;
	if (!lua_get_parameters(l, "%d%D%D%D", &b, &x, &y, &z)) {
		return luaL_error(l, "create_lua_block requires 4 integer arguments");
	}
	if (b < 1 || b > 9)	return luaL_error(l, "create_lua_block: block type must be between 1 and 9, not %ld", (long)b);
	LOCK(world) {
		set_block(x, y, z, b + LUA_1 - 1);
		record_block(x, y, z, b + LUA_1 - 1);
	}
	return 0;
}

static int lua_create_block(lua_State *l) {
	lua_Integer b, x, y, z;
	if (!lua_get_parameters(l, "%d%D%D%D", &b, &x, &y, &z)) {
		return luaL_error(l, "create_block requires 4 integer arguments");
	}
	if (b < 0 || b >= item_count)	return luaL_error(l, "create_block: block type %ld is not valid", (long)b);
	LOCK(world) {
		set_block(x, y, z, b);
		record_block(x, y, z, b);
	}
	return 0;
}

static int lua_get_block(lua_State *l) {
	lua_Integer x, y, z;
	if (!lua_get_parameters(l, "%D%D%D", &x, &y, &z)) {
		return luaL_error(l, "get_block requires 3 integer arguments");
	}
	int w;
	LOCK(world) {
		w = find_block_in_all_chunks(x, y, z);
	}
	return w;
}

static int lua_sleep(lua_State *l) {
	lua_Number s;
	if (!lua_get_parameters(l, "%f", &s) || s <= 0) {
		return luaL_error(l, "lua_sleep requires a positive number for an argument"); 
	}
#ifdef __MINGW32__
	Sleep((long)(s * 1000));
#else
	long sec = (long)s;
	struct timeval tv = { .tv_sec = sec, .tv_usec = (long)((s - sec) * 1000000) };
	select(0, NULL, NULL, NULL, &tv);
#endif
	return 0;
}

static int lua_get_player_position(lua_State *l) {
	LOCK(world) {
		lua_pushnumber(l, main_player->state.x);
		lua_pushnumber(l, main_player->state.y);
		lua_pushnumber(l, main_player->state.z);
	}
	return 3;
}

static int lua_set_player_position(lua_State *l) {
	lua_Number x, y, z;
	if (!lua_get_parameters(l, "%f%f%f", &x, &y, &z)) {
		return luaL_error(l, "set_player_position requires 3 numeric arguments (x, y, z)");
	}
	LOCK(world) {
		main_player->state.x = x;
		main_player->state.y = y;
		main_player->state.z = z;
	}
	return 0;
}

static int lua_get_where_pointing(lua_State *l) {
	int hx, hy, hz, hw, face;
	LOCK(world) {
		hit_test_face(main_player, &hx, &hy, &hz, &face);
		hw = hit_test(0, main_player->state.x, main_player->state.y,
			main_player->state.z, main_player->state.rx,
			main_player->state.ry, &hx, &hy, &hz);
	}
	lua_pushnumber(l, hw);
	lua_pushnumber(l, hx);
	lua_pushnumber(l, hy);
	lua_pushnumber(l, hz);
	lua_pushnumber(l, face);
	return 5;
}

static int lua_set_sign(lua_State *l) {
	lua_Integer x, y, z, face;
	char *text;
	if (!lua_get_parameters(l, "%D%D%D%d%s", &x, &y, &z, &face, &text)) {
		return luaL_error(l, "set_sign requires 5 arguments (x, y, z, face, text)");
	}
	extern void set_sign(int, int, int, int, char const *);
	set_sign(x, y, z, face, text);
	return 0;
}

static int lua_set_light(lua_State *l) {
	lua_Number x, y, z;
	lua_Integer w;
	if (!lua_get_parameters(l, "%f%f%f%d", &x, &y, &z, &w)) {
		return luaL_error(l, "set_light requires 4 arguments (x, y, z, light)");
	}
	if (w < 0) {
		w = 0;
	} else if (w > 15) {
		w = 15;
	}
	LOCK(world) {
		int p = chunked(x);
		int q = chunked(z);
		set_light(p, q, x, y, z, w);
	}
	return 0;
}

static int lua_get_light(lua_State *l) {
	lua_Number x, y, z;
	if (!lua_get_parameters(l, "%D%D%D", &x, &y, &z)) {
		return luaL_error(l, "get_light requires 3 arguments (x, y, z)\n");
	}
	LOCK(world) {
		int p = chunked(x);
		int q = chunked(z);
		lua_pushinteger(l, get_light(p, q, x, y, z));
	}
	return 1;
}

static int lua_advance_time_of_day(lua_State *l) {
	lua_Number t;
	if (!lua_get_parameters(l, "%f", &t)) {
		return luaL_error(l, "advance_time_of_day requires 1 argument\n");
	}
	LOCK(world) {
		extern float time_of_day_fudge;
		time_of_day_fudge += t;
	}
}

static int lua_get_time_of_day(lua_State *l) {
	LOCK(world) {
		extern float time_of_day();
		lua_pushnumber(l, time_of_day());
	}
	return 1;
}

static enum { INPUT_NOTHING, INPUT_STRING, INPUT_STRING_NEWLINE, INPUT_INT, INPUT_POS_INT, INPUT_NUM, INPUT_POS_NUM } input_mode;
static char typing_response[100];

void lua_craft_user_typed_something(char const *s) {
	strncpy(typing_response, s, sizeof typing_response);
	typing_response[sizeof typing_response - 1] = '\0';
	cnd_signal(&typing_release);
}

static int ok_num(lua_State *s) {
	char *end_ptr;
	long double r = strtold(typing_response, &end_ptr);
	if (*end_ptr == '\0') {
		lua_pushnumber(s, r);
		return 1;
	} else {
		return 0;
	}
}
static int ok_int(lua_State *s) {
	char *end_ptr;
	long long r = strtoll(typing_response, &end_ptr, 10);
	if (*end_ptr == '\0') {
		lua_pushinteger(s, r);
		return 1;
	} else {
		return 0;
	}
}
static int ok_string(lua_State *s) {
	lua_pushstring(s, typing_response);
	return 1;
}

// we try to match Lua's builtin io.read as closely as possible, with one extension:
//	*i	integer
//	*n	number
//	*l	string
//	*a	same as *l
// not supported yet: *L, multiple arguments
int lua_get_input(lua_State *l) {
	if (lua_gettop(l) == 0) {
		lua_pushstring(l, "*l");
	} else if (lua_gettop(l) == 2) {
		return luaL_error(l, "io.read does not support multiple arguments yet. This is a bug in Lua Craft. Sorry :(");
	}
	char *arg;
	if (!lua_get_parameters(l, "%s", &arg) != 0) {
		return luaL_error(l, "io.read requires a string argument");	// this should never happen
	}
#define IF_MODE(s, m, ok)	if (strcmp(arg, s) == 0) {	\
								do {	\
									input_mode = (m);	\
									mtx_lock(&typing_lock);	\
									typing = TYPING_LUA_RESPONSE;	\
									cnd_wait(&typing_release, &typing_lock);	\
									typing = NOT_TYPING;	\
									mtx_unlock(&typing_lock);	\
								} while (!ok(l));	\
								return 1;	\
							}
	IF_MODE("*n", INPUT_NUM, ok_num)
	else IF_MODE("*a", INPUT_STRING, ok_string)
	else IF_MODE("*l", INPUT_STRING, ok_string)
	else IF_MODE("*i", INPUT_INT, ok_int)
	else {
		return luaL_error(l, "io.read's argument must be *n, *i, *l or *a, not %s. Sorry", arg);
	}
#undef IF_MODE
}

void lua_craft_close(void) {
	if (lua)
		lua_close(lua);
}

static lua_State *lua_state_init(int file_number) {
	lua_State *lua = luaL_newstate();
	luaL_openlibs(lua);
	for (int i = 1; i <= 9; i++) {
		char block[8];
		sprintf(block, "block%d", i);
		lua_createtable(lua, 2, 4);
		lua_setglobal(lua, block);
	}
	lua_createtable(lua, 2, 8);
	lua_setglobal(lua, "game");
	#define LUA_FUNCTION(f, nm)	lua_pushcfunction(lua, f); lua_setglobal(lua, nm)
	LUA_FUNCTION(lua_add_message, "print");
	// io.read = lua_get_input
	lua_getglobal(lua, "io");
	lua_pushcfunction(lua, lua_get_input);
	lua_setfield(lua, -2, "read");
	lua_pop(lua, 1);	// rebalance the stack
	// end
	LUA_FUNCTION(lua_get_input, "io.read");
	LUA_FUNCTION(lua_set_textures, "set_textures");
	LUA_FUNCTION(lua_destroy_block, "destroy_block");
	LUA_FUNCTION(lua_create_lua_block, "create_lua_block");
	LUA_FUNCTION(lua_create_block, "create_block");
	LUA_FUNCTION(lua_set_light, "set_light");
	LUA_FUNCTION(lua_get_light, "get_light");
	LUA_FUNCTION(lua_get_block, "get_block");
	LUA_FUNCTION(lua_set_sign, "set_sign");
	LUA_FUNCTION(lua_sleep, "sleep");
	LUA_FUNCTION(lua_get_player_position, "get_player_position");
	LUA_FUNCTION(lua_set_player_position, "set_player_position");
	LUA_FUNCTION(lua_advance_time_of_day, "advance_time_of_day");
	LUA_FUNCTION(lua_get_time_of_day, "get_time_of_day");
	LUA_FUNCTION(lua_get_where_pointing, "get_where_pointing");
	#undef LUA_FUNCTION
	assert(file_number >= 1 && file_number <= 9);
	char filename[10];
	sprintf(filename, "lua/%d.lua", file_number);
	if (luaL_loadfile(lua, filename) != 0) {
		error(lua, filename);
		fprintf(stderr, "cannot load %s\n", filename);
		return NULL;
	} else if (lua_pcall(lua, 0, 0, 0) != 0) {
		error(lua, filename);
		return NULL;
	}
	return lua;
}

int lua_craft_init(int file_number) {
	lua_craft_close();	// just in case
	mtx_init(&world_lock, mtx_plain);
	mtx_init(&texture_lock, mtx_plain);
	mtx_init(&lua_state_lock, mtx_plain);
	cnd_init(&typing_release);
	lua = lua_state_init(file_number);
	if (!lua) {
		lua_craft_close();
		return -1;
	}
}



struct lua_pcall_thread {
	lua_State *lua;
	size_t lua_state_index;
	int num_args;
	char function_name[70];
};

static struct lua_pcall_thread *get_free_lua_state(void) {
	struct lua_pcall_thread *s = malloc(sizeof *s);
	s->lua = NULL;
	LOCK(lua_state) {
		s->lua = lua;
		s->lua_state_index = 0;
	}
	return s;
}

static void release_lua_state(struct lua_pcall_thread *arg) {
	printf("*** lua_gettop = %d\n", lua_gettop(arg->lua));
	free(arg);
}

static int lua_pcall_thread(void *arg_) {
	struct lua_pcall_thread *arg = arg_;
	int r = lua_pcall(arg->lua, arg->num_args, 0, 0);
	if (r != 0) {
		error(arg->lua, arg->function_name);
	}
	release_lua_state(arg);
	return r;
}

static int lua_function_in_thread(struct lua_pcall_thread *l, char const *fname, char const *format, va_list ap) {
	int num_args = 0;
	while (*format) {
		if (*format != '%') {
			// rebalance the stack
			lua_pop(l->lua, 1 + num_args);	// 1 to remove the function
			va_end(ap);
			return -1;
		}
		format++;
		switch (*format) {
		case 'd':	lua_pushinteger(l->lua, va_arg(ap, int));	break;
		case 's':	lua_pushstring(l->lua, va_arg(ap, char *));	break;
		case 'f':	lua_pushnumber(l->lua, va_arg(ap, double));	break;
		default:	lua_pop(l->lua, 1 + num_args); va_end(ap); return -1;
		}
		format++;
		num_args++;
	}
	thrd_t t;
	l->num_args = num_args;
	strcpy(l->function_name, fname);
	thrd_create(&t, lua_pcall_thread, l);
	thrd_detach(t);
	return 0;
}

static int lua_craft_block_function_v(int block_number, char const *function_name, char const *format, va_list ap) {
	char block[7];
	sprintf(block, "block%d", block_number);
	if (lua_getglobal(lua, block) != LUA_TTABLE) {
		lua_pop(lua, 1);
		return -1;
	}
	if (lua_getfield(lua, -1, function_name) != LUA_TFUNCTION) {
		lua_pop(lua, 2);
		return -1;
	}
	lua_remove(lua, 1);
	char fname[70];
	sprintf(fname, "block%d.%s", block_number, function_name);
	return lua_function_in_thread(get_free_lua_state(), fname, format, ap);
}

int lua_craft_block_function(int block_number, char const *function_name, char const *format, ...) {
	va_list ap;
	va_start(ap, format);
	int r = lua_craft_block_function_v(block_number, function_name, format, ap);
	va_end(ap);
	return r;
}

static int lua_craft_game_function_v(char const *function_name, char const *format, va_list ap) {
	if (lua_getglobal(lua, "game") != LUA_TTABLE) {
		lua_pop(lua, 1);
		return -1;
	}
	if (lua_getfield(lua, -1, function_name) != LUA_TFUNCTION) {
		lua_pop(lua, 2);
		return -1;
	}
	lua_remove(lua, 1);
	char fname[70];
	sprintf(fname, "game.%s", function_name);
	return lua_function_in_thread(get_free_lua_state(), fname, format, ap);
}

int lua_craft_game_function(char const *function_name, char const *format, ...) {
	va_list ap;
	va_start(ap, format);
	int r = lua_craft_game_function_v(function_name, format, ap);
	va_end(ap);
	return r;
}
