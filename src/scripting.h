#include <GL/glew.h>

int lua_craft_init(int);
void lua_craft_close(void);
int lua_craft_block_function(int, char const *, char const *, ...);
int lua_craft_game_function(char const *, char const *, ...);
void lua_craft_user_typed_something(char const *);

// these are defined in main.c, but we need access to them from some Lua functions
typedef struct {
    float x;
    float y;
    float z;
    float rx;
    float ry;
    float t;
} State;

#define MAX_NAME_LENGTH 32
typedef struct {
    int id;
    char name[MAX_NAME_LENGTH];
    State state;
    State state1;
    State state2;
    GLuint buffer;
} Player;
extern Player *main_player;

struct Chunk;
typedef struct Chunk Chunk;


// functions defined in main.c that we need to reference from Lua scripts (indirectly)
void add_message(char const *);
void set_block(int, int, int, int);
void record_block(int, int, int, int);
void set_light(int, int, int, int, int, int);
int get_light(int, int, int, int, int);
Chunk *find_chunk(int, int);
void dirty_chunk(Chunk *);
int chunked(float);
int find_block_in_all_chunks(int, int, int);
int hit_test(int, float, float, float, float, float, int *, int *, int *);
int hit_test_face(Player *, int *, int *, int *, int *);
void reset_map(void);

