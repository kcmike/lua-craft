#include "io.h"
#include <string.h>
#include <stdio.h>

int message_index;
char messages[MAX_MESSAGES][MAX_TEXT_LENGTH];
enum typing_mode typing;
char typing_buffer[MAX_TEXT_LENGTH];
mtx_t typing_lock, message_lock;

void init_io(void) {
	mtx_init(&message_lock, mtx_plain);
	mtx_init(&typing_lock, mtx_plain);
	memset(typing_buffer, 0, sizeof(char) * MAX_TEXT_LENGTH);
	typing = 0;
	memset(messages, 0, sizeof(char) * MAX_MESSAGES * MAX_TEXT_LENGTH);
	message_index = 0;
}

void add_message(const char *text) {
	printf("%s\n", text);
	mtx_lock(&message_lock);
	strncpy(messages[message_index], text, MAX_TEXT_LENGTH);
	messages[message_index][MAX_TEXT_LENGTH - 1] = '\0';
	message_index = (message_index + 1) % MAX_MESSAGES;
	mtx_unlock(&message_lock);
}

