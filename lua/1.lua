block1.on_create = function (x, y, z)
	print(string.format('I was placed at (%d, %d, %d)', x, y, z))
end
block2.on_left_click = function (x, y, z)
	destroy_block(x, y, z)
	sleep(1)
	create_block(2, x + 1, y, z)
	sleep(1)
	create_block(2, x, y + 1, z)
end
block3.on_left_click = function (x, y, z)
	px, py, pz = get_player_position()
	set_player_position(px + 1, py, pz)
end
